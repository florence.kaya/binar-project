const express = require("express");
const app = express();
const port = 8000;

app.set("view engine", "ejs");
app.use(express.static("public"));


app.get("/",(req, res) => {
    res.render("homepage");
});

app.get("/home",(req, res) => {
    res.render("homepage");
});

app.get("/games", (req, res) => {
    res.render("thegame");
})


app.listen(port, () => console.log(`Web App Up in http://localhost:${port}`));