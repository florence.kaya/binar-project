const express = require("express");
const app = express();
const port = 8000;

let posts = require("./posts.json");
app.get("/api/posts", (req,res) => {
    res.status(200).json(posts);
});

app.use(express.json());

app.post("/api/posts/signup",(req, res) => {
    const { name, username, password } = req.body;
    const id = posts[posts.length - 1].id + 1;
    const post = { id,name, username, password};
    posts.push(post);
    res.status(201).json(post);

    });
app.post("/api/posts/login", (req, res) => {
        let post = posts.find(function (post) {
            return post.username === req.body.username;
        });
    
        if (!req.body.username) {
            res.status(401).json("Email salah");
            return;
        }
        if (!req.body.password) {
            res.status(401).json("password salah")
            return;
        }
        res.status(200).json(post);
    
    });

app.listen(port, () => console.log(`Web App Up in http://localhost:${port}`));